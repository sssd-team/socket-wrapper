#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include "config.h"
#include "torture.h"

#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

static int setup_echo_srv_tcp_ipv4(void **state)
{
	torture_setup_echo_srv_tcp_ipv4(state);

	return 0;
}

#ifdef HAVE_IPV6
static int setup_echo_srv_tcp_ipv6(void **state)
{
	torture_setup_echo_srv_tcp_ipv6(state);

	return 0;
}
#endif

static int teardown(void **state)
{
	torture_teardown_echo_srv(state);

	return 0;
}

static void test_write_read_ipv4_size(void **state, size_t size)
{
	struct torture_address addr = {
		.sa_socklen = sizeof(struct sockaddr_in),
	};
	char send_buf[size];
	char recv_buf[size];
	ssize_t ret;
	int rc;
	int i;
	int s;

	(void) state; /* unused */

	s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	assert_int_not_equal(s, -1);

	addr.sa.in.sin_family = AF_INET;
	addr.sa.in.sin_port = htons(torture_server_port());

	rc = inet_pton(addr.sa.in.sin_family,
		       torture_server_address(AF_INET),
		       &addr.sa.in.sin_addr);
	assert_int_equal(rc, 1);

	rc = connect(s, &addr.sa.s, addr.sa_socklen);
	assert_int_equal(rc, 0);

	memset(send_buf, 0, sizeof(send_buf));
	for (i = 0; i < 10; i++) {
		size_t nread = 0, nwrote = 0;
		snprintf(send_buf, sizeof(send_buf), "packet.%d", i);

		do {
			ret = write(s,
				    send_buf + nwrote,
				    sizeof(send_buf) - nwrote);
			assert_int_not_equal(ret, -1);
			nwrote += ret;

			ret = read(s,
				   recv_buf + nread,
				   sizeof(recv_buf) - nread);
			assert_int_not_equal(ret, -1);
			nread += ret;
		} while (nread < sizeof(recv_buf) && nwrote < sizeof(send_buf));

		assert_int_equal(nread, sizeof(send_buf));
		assert_memory_equal(send_buf, recv_buf, sizeof(send_buf));
	}

	close(s);
}
static void test_write_read_ipv4(void **state)
{
	test_write_read_ipv4_size(state, 64);
}

static void test_write_read_ipv4_large(void **state)
{
	test_write_read_ipv4_size(state, 2000);
}

#ifdef HAVE_IPV6
static void test_write_read_ipv6_size(void **state, size_t size)
{
	struct torture_address addr = {
		.sa_socklen = sizeof(struct sockaddr_in6),
	};
	char send_buf[size];
	char recv_buf[size];
	ssize_t ret;
	int rc;
	int i;
	int s;

	(void) state; /* unused */

	s = socket(AF_INET6, SOCK_STREAM, IPPROTO_TCP);
	assert_int_not_equal(s, -1);

	addr.sa.in6.sin6_family = AF_INET6;
	addr.sa.in6.sin6_port = htons(torture_server_port());

	rc = inet_pton(AF_INET6,
		       torture_server_address(AF_INET6),
		       &addr.sa.in6.sin6_addr);
	assert_int_equal(rc, 1);

	rc = connect(s, &addr.sa.s, addr.sa_socklen);
	assert_int_equal(rc, 0);

	memset(send_buf, 0, sizeof(send_buf));
	for (i = 0; i < 10; i++) {
		size_t nread = 0, nwrote = 0;
		snprintf(send_buf, sizeof(send_buf), "packet.%d", i);

		do {
			ret = write(s,
				    send_buf + nwrote,
				    sizeof(send_buf) - nwrote);
			assert_int_not_equal(ret, -1);
			nwrote += ret;

			ret = read(s,
				   recv_buf + nread,
				   sizeof(recv_buf) - nread);
			assert_int_not_equal(ret, -1);
			nread += ret;
		} while (nread < sizeof(recv_buf) && nwrote < sizeof(send_buf));

		assert_int_equal(nread, sizeof(send_buf));
		assert_memory_equal(send_buf, recv_buf, sizeof(send_buf));
	}

	close(s);
}

static void test_write_read_ipv6(void **state)
{
	test_write_read_ipv6_size(state, 64);
}

static void test_write_read_ipv6_large(void **state)
{
	test_write_read_ipv6_size(state, 2000);
}
#endif

int main(void) {
	int rc;

	const struct CMUnitTest tcp_write_tests[] = {
		cmocka_unit_test_setup_teardown(test_write_read_ipv4,
						setup_echo_srv_tcp_ipv4,
						teardown),
		cmocka_unit_test_setup_teardown(test_write_read_ipv4_large,
						setup_echo_srv_tcp_ipv4,
						teardown),
#ifdef HAVE_IPV6
		cmocka_unit_test_setup_teardown(test_write_read_ipv6,
						setup_echo_srv_tcp_ipv6,
						teardown),
		cmocka_unit_test_setup_teardown(test_write_read_ipv6_large,
						setup_echo_srv_tcp_ipv6,
						teardown),
#endif
	};

	rc = cmocka_run_group_tests(tcp_write_tests, NULL, NULL);

	return rc;
}
